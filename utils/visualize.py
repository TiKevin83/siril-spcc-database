import json
import matplotlib.pyplot as plt
import sys

def plot_from_json(filename, output_file=None):
    # Load JSON data from the file
    with open(filename, 'r') as file:
        data = json.load(file)

    # Initialize the figure
    plt.figure()

    # Iterate over each data block in the JSON file
    for dataset in data:
        # Extract values from the current data block
        name = dataset['name']
        wavelength = dataset['wavelength']['value']
        wavelength_units = dataset['wavelength']['units'] if 'units' in dataset['wavelength'] else 'nm'
        values = dataset['values']['value']

        # Plot the curve for the current data block
        plt.plot(wavelength, values, label=name)

    # Add labels and a legend
    plt.xlabel(f'Wavelength / {wavelength_units}')
    plt.ylabel('Transmittance')
    plt.title('Plot')
    plt.grid(True)
    plt.legend()

    # Save the plot to disk if output_file is provided
    if output_file:
        plt.savefig(output_file)
    else:
        plt.show()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python visualize.py <filename> [output_file]")
        sys.exit(1)
    filename = sys.argv[1]
    output_file = sys.argv[2] if len(sys.argv) > 2 else None
    plot_from_json(filename, output_file)

